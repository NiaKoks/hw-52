import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Lottery from "./components/lottery";

class App extends Component {


    state = {
        numbers: []
    };


    getRandomNumbers = () => {
      let numbers = [];

      while (numbers.length < 5) {
          let number = Math.floor(Math.random() * (36 - 5) + 5);
          if (!numbers.includes(number)) numbers.push(number);
      }

      numbers = numbers.sort((a, b) => a - b);
      this.setState({numbers})
    };

    clickHandler = () => {
      this.getRandomNumbers()
    };

    render() {
        return (
            <div className="Lottery-Box">
              <button onClick={this.clickHandler} className="rand-btn">new number</button>
                {this.state.numbers.map((number) => {
                  return <Lottery number={number}/>
                    // return <div className="CircleForNumber"> <Lottery> {number}</Lottery> </div>
                })}
            </div>
        )
    }
}

export default App;
